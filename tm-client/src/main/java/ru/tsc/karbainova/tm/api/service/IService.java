package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.api.repository.IRepository;
import ru.tsc.karbainova.tm.endpoint.AbstractEntity;


import java.util.List;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

    void addAll(List<E> entities);

    void clear();

    void remove(E entity);
}
