package ru.tsc.karbainova.tm.command.auth;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.endpoint.Session;
import ru.tsc.karbainova.tm.endpoint.User;

public class AuthViewProfileCommand extends AbstractCommand {
    @Override
    public String name() {
        return "show-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "View Profile";
    }

    @Override
    public void execute() {
        Session session = serviceLocator.getSession();
        final User user = serviceLocator.getAuthEndpoint().getUser(session);
        System.out.println("Login: " + user.getLogin());
        System.out.println("Email: " + user.getEmail());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Middle name: " + user.getMiddleName());
    }
}
