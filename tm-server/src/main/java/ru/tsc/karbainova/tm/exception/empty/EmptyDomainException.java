package ru.tsc.karbainova.tm.exception.empty;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class EmptyDomainException extends AbstractException {
    public EmptyDomainException() {
        super("Error");
    }

    public EmptyDomainException(String value) {
        super("Error " + value);
    }
}
