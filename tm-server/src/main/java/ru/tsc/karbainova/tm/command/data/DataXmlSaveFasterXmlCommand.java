package ru.tsc.karbainova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;

import java.io.FileOutputStream;

public class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {
    @Override
    public String name() {
        return "xml-faster-save";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Xml faster save";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NonNull final Domain domain = getDomain();
        @NonNull final ObjectMapper objectMapper = new XmlMapper();
        @NonNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NonNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
