package ru.tsc.karbainova.tm.service;

import java.util.Comparator;
import java.util.List;

import ru.tsc.karbainova.tm.api.repository.IRepository;
import ru.tsc.karbainova.tm.api.service.IService;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.model.AbstractEntity;
import ru.tsc.karbainova.tm.model.AbstractOwnerEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {
    protected IRepository repository;

    public AbstractService(IRepository repository) {
        this.repository = repository;
    }

    @Override
    public void addAll(List<E> entities) {
        if (entities == null) return;
        repository.addAll(entities);
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public void remove(E entity) {
        repository.remove(entity);
    }

    @Override
    public E add(E entity) {
        if (entity == null) return null;
        return (E) repository.add(entity);
    }

    @Override
    public boolean exists(String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.exists(id);
    }
}
